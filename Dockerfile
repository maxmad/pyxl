FROM registry.gitlab.com/maxmad/ipython:latest

COPY requirements.txt /requirements.txt

RUN pip install --no-cache-dir -r /requirements.txt

WORKDIR /usr/src/app

COPY ./app /usr/src/app

CMD ["ipython"]
