# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import logging

from requests import Session
from requests.adapters import BaseAdapter
from requests.auth import HTTPBasicAuth
from requests.models import Response
from suds.properties import Unskin
from suds.transport import Transport, Reply

from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

# from suds.transport import TransportError


handler = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s %(levelname)-8s %(message)s')
handler.setFormatter(formatter)

log = logging.getLogger(__name__)
log.addHandler(handler)
log.setLevel(logging.DEBUG)


class FileAdapter(BaseAdapter):
    def send(self, request, **kwargs):
        response = Response()
        response.headers = {}
        response.encoding = 'utf-8'  # FIXME: this is a complete guess
        response.url = request.url
        response.request = request
        response.connection = self

        try:
            response.raw = open(request.url.replace('file://', ''), 'rb')
        except IOError:
            response.status_code = 404
            return response

        response.status_code = 200
        return response

    def close(self):
        pass


class RequestsHttpTransport(Transport):
    def __init__(self, session=None, **kwargs):
        super(RequestsHttpTransport, self).__init__()
        Unskin(self.options).update(kwargs)
        self.session = session or Session()
        self.session.mount('file://', FileAdapter())

        retries = Retry(connect=5, read=5, backoff_factor=0.5)

        self.session.mount('http://', HTTPAdapter(max_retries=retries))
        self.session.mount('https://', HTTPAdapter(max_retries=retries))

    def _call(self, request, method):
        headers = dict(self.options.headers)
        headers.update(request.headers)
        if self.options.username and self.options.password:
            auth = HTTPBasicAuth(self.options.username, self.options.password)
        else:
            auth = None

        try:
            response = getattr(self.session, method)(request.url,
                                                     auth=auth,
                                                     data=request.message,
                                                     headers=headers,
                                                     timeout=self.options.timeout,
                                                     proxies=self.options.proxy)
        except Exception as e:
            log.error(e)
            raise e

#        if response.status_code >= 400:
#            raise TransportError(response.content, response.status_code)

        return response

    def open(self, request):
        return self._call(request, 'get').raw

    def send(self, request):
        response = self._call(request, 'post')
        return Reply(response.status_code, response.headers, response.content)
