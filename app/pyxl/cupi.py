import os
import requests

CUC_HOST = os.environ['CUC_HOST']
CUC_USER = os.environ['CUC_USER']
CUC_PASS = os.environ['CUC_PASS']
CUC_PASS = 'D395B2Vh'


class CUPI(object):

    def __init__(self,
                 host=CUC_HOST,
                 username=CUC_USER,
                 password=CUC_PASS,
                 verify=False,
                 disable_warnings=False,
                 timeout=5):

        self.url_base = f'https://{host}/vmrest'
        self.cuc = requests.session()
        self.cuc.auth = (username, password)
        self.cuc.verify = verify
        self.disable_warnings = disable_warnings
        self.timeout = timeout
        self.cuc.headers.update({
            'Accept': 'application/json',
            'Connection': 'keep_alive',
            'Content_type': 'application/json',
        })

        if self.disable_warnings:
            requests.packages.urllib3.disable_warnings()

    def import_ldap_user(self, alias, template="voicemailusertemplate"):

        # find user in ldap
        url = f'{self.url_base}/import/users/ldap?query=(alias is {alias})'
        resp = self.cuc.get(url, timeout=self.timeout)

        # import user if found
        url = f'{self.url_base}/import/users/ldap?templateAlias={template}'
        if resp.json()['@total'] is '1':
            user = resp.json()['ImportUser']
            return self.cuc.post(url, json=user, timeout=self.timeout)

    def reset_user_pin(self, alias, pin):
        url = f'{self.url_base}/users?query=(alias is {alias})'
        user = self.cuc.get(url, timeout=self.timeout)
        if user.json()['@total'] is '1':
            objectid = user.json()['User']['ObjectId']
            url = f'{self.url_base}/users/{objectid}/credential/pin'
            data = {'Credentials': pin}
            resp = self.cuc.put(url, json=data, timeout=self.timeout)

            if resp.status_code is 204:
                return 'PIN changed succesfully'
            else:
                return resp.json()
