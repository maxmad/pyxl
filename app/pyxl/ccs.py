
import os
import requests
import ssl
from datetime import timedelta
from requests.packages.urllib3.exceptions import InsecureRequestWarning
from suds.client import Client
from suds.plugin import MessagePlugin
from suds.xsd.doctor import Import
from suds.xsd.doctor import ImportDoctor

from pyxl.defaults import default_services

try:
    _create_unverified_https_context = ssl._create_unverified_context
except AttributeError:
    pass
else:
    ssl._create_default_https_context = _create_unverified_https_context


cucmhost = os.environ['CUCM_HOST']
cucmuser = os.environ['CUCM_USER']
cucmpass = os.environ['CUCM_PASS']
cucmvers = os.environ['CUCM_VERSION']

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


class CCS(object):
    """docstring for CCS"""

    def __init__(
            self,
            host=cucmhost,
            username=cucmuser,
            password=cucmpass,
            timeout=30):

        self.host = host
        self.username = username
        self.password = password

        wsdl = f'https://{host}/controlcenterservice2/services/ControlCenterServices?wsdl'
        location = f'https://{host}/controlcenterservice2/services/ControlCenterServices'

        tns = 'http://schemas.cisco.com/ast/soap'
        imp = Import('http://schemas.xmlsoap.org/soap/encoding/')
        imp.filter.add(tns)

        class SetPrefix(MessagePlugin):

            def marshalled(self,
                           context):
                context.envelope[1][0][0].setPrefix('soap')

        self.client = Client(
            wsdl,
            location=location,
            username=username,
            password=password,
            timeout=timeout,
            plugins=[ImportDoctor(imp), SetPrefix()]
        )

    def getProductInformationList(self):
        getProductInformationList = self.client.factory.create(
            'getProductInformationList')
        getProductInformationList.ServiceInfo = ""
        result = self.client.service.getProductInformationList(
            getProductInformationList)
        return list(result)

    def soapGetServiceStatus(self, services='', table=False, full=False):

        soapGetServiceStatus = self.client.factory.create(
            'soapGetServiceStatus')

        if not isinstance(services, list):
            services = [services]

        soapGetServiceStatus.ServiceStatus = services

        try:
            response = self.client.service.soapGetServiceStatus(
                soapGetServiceStatus)
        except Exception as e:
            response = None
            print(e)
            return response

        if table:
            servs = []

            if full:
                for s in response[3][0]:
                    servs.append({
                        'ServiceName': s['ServiceName'],
                        'StartTime': s['StartTime'],
                        'UpTime': str(timedelta(seconds=s['UpTime']))
                    })
            else:
                for s in response[3][0]:
                    if str(s['ServiceName']) not in default_services:
                        servs.append({
                            'ServiceName': s['ServiceName'],
                            'StartTime': s['StartTime'],
                            'UpTime': str(timedelta(seconds=s['UpTime']))
                        })
            return servs

        return response

    def soapDoServiceDeployment(self, node, control, services):
        '''
        # control:
        Deploy
        UnDeploy

        # CUC Services:
            Cisco DirSync
            Cisco Prime LM Resource API
            Cisco Prime LM Resource Legacy API
            Cisco Serviceability Reporter
            Connection Access Layer
            Connection Branch Sync Service
            Connection CM Database Event Listener
            Connection Conversation Manager
            Connection Database Proxy
            Connection Diagnostic Portal Service
            Connection Digital Networking Replication Agent
            Connection Directory Feeder
            Connection Exchange Notification Web Service
            Connection File Syncer
            Connection Groupware Caching Service
            Connection HTTPS Directory Feeder
            Connection IMAP Server
            Connection Inbox RSS Feed
            Connection Integrated Mailbox Configuration
            Connection Jetty
            Connection Mailbox Sync
            Connection Message Event Service
            Connection Message Transfer Agent
            Connection Mixer
            Connection Notifier
            Connection Personal Communication Assistant
            Connection Realtime Monitoring APIs
            Connection Reports Data Harvester
            Connection REST Service
            Connection SMTP Server
            Connection SpeechView Processor
            Connection System Agent
            Connection Voice Mail Web Service
            Connection Voice Recognition Transport
            Connection Voice Recognizer

        # IMP Services:
            A Cisco DB Replicator
            Cisco AMC Service
            Cisco Audit Event Service
            Cisco AXL Web Service
            Cisco Bulk Provisioning Service
            Cisco CallManager Serviceability
            Cisco CallManager Serviceability RTMT
            Cisco CDP
            Cisco CDP Agent
            Cisco Certificate Expiry Monitor
            Cisco Client Profile Agent
            Cisco Config Agent
            Cisco Database Layer Monitor
            Cisco DRF Local
            Cisco IM and Presence Admin
            Cisco IM and Presence Data Monitor
            Cisco Intercluster Sync Agent
            Cisco Log Partition Monitoring Tool
            Cisco Login Datastore
            Cisco Management Agent Service
            Cisco OAM Agent
            Cisco Presence Datastore
            Cisco Presence Engine
            Cisco RCC Device Selection Service
            Cisco RIS Data Collector
            Cisco Route Datastore
            Cisco RTMT Reporter Servlet
            Cisco Server Recovery Manager
            Cisco Serviceability Reporter
            Cisco SIP Proxy
            Cisco SIP Registration Datastore
            Cisco Sync Agent
            Cisco Syslog Agent
            Cisco Trace Collection Service
            Cisco Trace Collection Servlet
            Cisco XCP Authentication Service
            Cisco XCP Config Manager
            Cisco XCP Connection Manager
            Cisco XCP Directory Service
            Cisco XCP File Transfer Manager
            Cisco XCP Message Archiver
            Cisco XCP Router
            Cisco XCP SIP Federation Connection Manager
            Cisco XCP Text Conference Manager
            Cisco XCP Web Connection Manager
            Cisco XCP XMPP Federation Connection Manager
            Host Resources Agent
            MIB2 Agent
            Platform Administrative Web Service
            SNMP Master Agent
            SOAP -Log Collection APIs
            SOAP -Performance Monitoring APIs
            SOAP -Real-Time Service APIs
            System Application Agent

        # CUCM Services:
            A Cisco DB Replicator
            Cisco AMC Service
            Cisco Audit Event Service
            Cisco Bulk Provisioning Service
            Cisco CallManager
            Cisco CallManager Admin
            Cisco CallManager Serviceability
            Cisco CallManager Serviceability RTMT
            Cisco CallManager SNMP Service
            Cisco CAR DB
            Cisco CAR Scheduler
            Cisco CAR Web Service
            Cisco CDP
            Cisco CDP Agent
            Cisco CDR Agent
            Cisco CDR Repository Manager
            Cisco Certificate Authority Proxy Function
            Cisco Certificate Change Notification
            Cisco Certificate Expiry Monitor
            Cisco Change Credential Application
            Cisco CTIManager
            Cisco CTL Provider
            Cisco Database Layer Monitor
            Cisco DHCP Monitor Service
            Cisco Dialed Number Analyzer
            Cisco Dialed Number Analyzer Server
            Cisco Directory Number Alias Lookup
            Cisco Directory Number Alias Sync
            Cisco DirSync
            Cisco DRF Local
            Cisco DRF Master
            Cisco E911
            Cisco ELM Client Service
            Cisco Extended Functions
            Cisco Extension Mobility
            Cisco Extension Mobility Application
            Cisco Intercluster Lookup Service
            Cisco IP Manager Assistant
            Cisco IP Voice Media Streaming App
            Cisco License Manager
            Cisco Location Bandwidth Manager
            Cisco Log Partition Monitoring Tool
            Cisco Management Agent Service
            Cisco Prime LM Admin
            Cisco Prime LM DB
            Cisco Prime LM Resource API
            Cisco Prime LM Resource Legacy API
            Cisco Prime LM Server
            Cisco Push Notification Service
            Cisco RIS Data Collector
            Cisco RTMT Reporter Servlet
            Cisco Serviceability Reporter
            Cisco SOAP - CallRecord Service
            Cisco SOAP - CDRonDemand Service
            Cisco Syslog Agent
            Cisco TAPS Service
            Cisco Tftp
            Cisco Trace Collection Service
            Cisco Trace Collection Servlet
            Cisco Trust Verification Service
            Cisco Unified Mobile Voice Access Service
            Cisco User Data Services
            Cisco UXL Web Service
            Cisco WebDialer Web Service
            Cisco Wireless Controller Synchronization Service
            Host Resources Agent
            MIB2 Agent
            Platform Administrative Web Service
            Self Provisioning IVR
            SNMP Master Agent
            SOAP - Diagnostic Portal Database Service
            SOAP -Log Collection APIs
            SOAP -Performance Monitoring APIs
            SOAP -Real-Time Service APIs
            System Application Agent
        '''

        soapDoServiceDeployment = self.client.factory.create(
            'soapDoServiceDeployment')

        soapDoServiceDeployment.DeploymentServiceRequest.NodeName = node
        soapDoServiceDeployment.DeploymentServiceRequest.DeployType = control

        for service in services:
            soapDoServiceDeployment.DeploymentServiceRequest.ServiceList.item.append(
                service)

        result = self.client.service.soapDoServiceDeployment(
            soapDoServiceDeployment)

        return list(result)

    def soapDoControlServices(self, node, control, services):
        '''
        # control:
        Start
        Stop
        Restart
        '''

        if not isinstance(services, list):
            services = [services]

        soapDoControlServices = self.client.factory.create(
            'soapDoControlServices')

        soapDoControlServices.ControlServiceRequest.NodeName = node
        soapDoControlServices.ControlServiceRequest.ControlType = control

        for service in services:
            soapDoControlServices.ControlServiceRequest.ServiceList.item.append(
                service)

        result = self.client.service.soapDoControlServices(
            soapDoControlServices)

        return list(result)
