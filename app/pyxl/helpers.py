import logging

from suds.sudsobject import asdict
from suds.sax.text import Text
import csv
import os
import secrets
import hashlib
from base64 import b64encode


def get_module_logger(mod_name, level=None):
    """
    To use this, do logger = get_module_logger(__name__)
    """
    if not level:
        level = 'DEBUG'
    logger = logging.getLogger(mod_name)
    handler = logging.StreamHandler()
    formatter = logging.Formatter('%(asctime)s %(levelname)-8s %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(level)
    return logger


log = get_module_logger(__name__)
log.debug(f'module {__name__} initiated.')


def pw_gen():
    char_classes = ("abcdefghjklmnpqrstuvwxyz",
                    "abcdefghjklmnpqrstuvwxyz".upper(),
                    '0123456789')

    # password length between 8 and 13 chars
    def size(): return secrets.choice(range(8, 13))

    def char(): return secrets.choice(secrets.choice(char_classes))

    def pw(): return ''.join([char() for _ in range(size())])

    return pw()


def pw_ssha(password):
    password = password.encode('utf-8')
    salt = os.urandom(8)
    h = hashlib.sha1(password)
    h.update(salt)
    return "{SSHA}" + str(b64encode(h.digest() + salt))[2:-1]


def read_csv(filename="userlist.csv", header=True):
    with open(filename, newline='') as f:
        reader = csv.reader(f, delimiter=';')
        if header:
            next(reader, None)
            return [row for row in reader]
        return [row for row in reader]


def gen_response(resp):
    if resp[0] == 200:
        if resp[1]['return'] == '':
            return None
        x = resp[1]['return'].__keylist__[0]
        return resp[1]['return'][x]
    else:
        log.error(
            f'{resp[1]["detail"]["axlError"]["request"]}: {resp[1]["detail"]["axlError"]["axlmessage"]}')


def response(resp):
    result = None

    if resp[0] == 200:
        if isinstance(resp[1]['return'], str):
            result = resp[1]['return']
        else:
            x = resp[1]['return'].__keylist__[0]
            result = resp[1]['return'][x]
    else:
        log.error(
            f'{resp[1]["detail"]["axlError"]["request"]}: {resp[1]["detail"]["axlError"]["axlmessage"]}')
        result = resp[1]['detail']

    return result


def next_response(resp):
    if resp[0] != 200:
        log.error(
            f'{resp[1]["detail"]["axlError"]["request"]}: {resp[1]["detail"]["axlError"]["axlmessage"]}')
    else:
        return resp[1]


def gen_ascii(text):
    text = text.replace(u"Ä", u"ae")
    text = text.replace(u"ä", u"Ae")
    text = text.replace(u"Ö", u"Oe")
    text = text.replace(u"ö", u"oe")
    text = text.replace(u"Ü", u"Ue")
    text = text.replace(u"ü", u"ue")
    text = text.replace(u"ß", u"ss")
    return text


def sudsToDict(data):
    return dict([(str(key), val) for key, val in data])


def recursive_asdict(d):
    """Convert Suds object into serializable format."""
    out = {}
    for k, v in asdict(d).items():
        if hasattr(v, '__keylist__'):
            out[k] = recursive_asdict(v)
        elif isinstance(v, list):
            out[k] = []
            for item in v:
                if hasattr(item, '__keylist__'):
                    out[k].append(recursive_asdict(item))
                else:
                    out[k].append(item)
        else:
            out[k] = v
    return out


def suds2dict(d, clean=True):
    """
    Convert Suds object into serializable format.
    if clean=True, removes (_sequence,)
    """
    out = {}
    for k, v in asdict(d).items():
        if hasattr(v, '__keylist__'):
            out[k] = suds2dict(v)
        elif isinstance(v, list):
            out[k] = []
            for item in v:
                if hasattr(item, '__keylist__'):
                    out[k].append(suds2dict(item))
                else:
                    out[k].append(item)
        else:
            if v != '_sequence':
                out[k] = v
    return out


def sudsres2dict(obj):
    """
    Converts a suds object to a dict.
    :param obj: suds object
    :return: dict object
    """
    if not hasattr(obj, '__keylist__'):
        return obj

    data = {}
    fields = obj.__keylist__
    for field in fields:
        if hasattr(obj, 'value'):
            data = obj.value
        elif field not in ('_uuid', '_ctiid'):
            val = getattr(obj, field)
            if isinstance(val, list):
                data[field] = []
                for item in val:
                    data[field].append(sudsres2dict(item))
            else:
                if val:
                    data[field] = sudsres2dict(val)
    return data


def sobject_to_dict(obj):
    """
    Converts a suds object to a dict.
    :param obj: suds object
    :return: dict object
    """
    if not hasattr(obj, '__keylist__'):
        return obj

    data = {}
    fields = obj.__keylist__
    for field in fields:
        val = getattr(obj, field)
        if isinstance(val, list):
            data[field] = []
            for item in val:
                data[field].append(sobject_to_dict(item))
        else:
            if val:
                data[field] = sobject_to_dict(val)
    return data


def dict_compare(d1, d2):
    d1_keys = set(d1.keys())
    d2_keys = set(d2.keys())
    intersect_keys = d1_keys.intersection(d2_keys)
    added = d1_keys - d2_keys
    removed = d2_keys - d1_keys
    modified = {o: (d1[o], d2[o]) for o in intersect_keys if d1[o] != d2[o]}
    modified1 = {o: (d2[o]) for o in intersect_keys if d1[o] != d2[o]}
    same = set(o for o in intersect_keys if d1[o] == d2[o])
    return added, removed, modified, modified1, same


def dict_modified(d1, d2):
    d1_keys = set(d1.keys())
    d2_keys = set(d2.keys())
    intersect_keys = d1_keys.intersection(d2_keys)
    modified = {o: (d2[o]) for o in intersect_keys if d1[o] != d2[o]}
    return modified


def pprint_sql(sql):
    import sqlparse
    return sqlparse.format(sql, reindent=True, keyword_case='upper')


def factorysudsToDict(data, update=False):
    return dict([(str(key), val) for key, val in data if val])


def create_ccm_link(ip, linktype, name, uuid):
    return ('<p><a target="_self" crossorigin="anonymous" href="https://{0}/ccmadmin/{1}Edit.do?key={2}">{3}</a></p>').format(ip, linktype, uuid.lower(), name)


def nice_response(d):
    out = {}
    for k, v in asdict(d).items():
        if hasattr(v, '__keylist__'):
            if hasattr(v, 'value'):
                out[k] = v.value
            else:
                out[k] = nice_response(v)
        elif isinstance(v, list):
            out[k] = []
            for item in v:
                if hasattr(item, '__keylist__'):
                    out[k].append(nice_response(item))
                else:
                    out[k].append(item)
        else:
            out[k] = v
    return out


def remove_empty_tags(data):
    r = data
    for i in r.__keylist__:
        if r[i] == "":
            r.__delattr__(i)
    return r


def remove_default_tags(d):
    out = {}
    for k, v in asdict(d).items():
        if k not in ('_ctiid', '_uuid'):
            if v:
                if v not in ('None', '', 'Default'):
                    if hasattr(v, '__keylist__'):
                        if hasattr(v, 'value'):
                            if v.value:
                                if v.value not in ('None', '', 'Default'):
                                    out[k] = v.value
                        else:
                            out[k] = remove_default_tags(v)
                    elif isinstance(v, list):
                        out[k] = []
                        for item in v:
                            if hasattr(item, '__keylist__'):
                                out[k].append(remove_default_tags(item))
                            else:
                                if item not in ('None', '', 'Default'):
                                    if item:
                                        out[k].append(item)
                    else:
                        out[k] = v
    return out


def buildRequest(data):

    def createDict(data):
        out = {}
        for k, v in asdict(data).items():
            if hasattr(v, '__keylist__'):
                out[k] = createDict(v)
            elif isinstance(v, list) and v:
                out[k] = []
                for item in v:
                    if hasattr(item, '__keylist__'):
                        out[k].append(createDict(item))
                    else:
                        out[k].append(
                            item.title() if isinstance(item, Text) else item)
            else:
                if v:
                    out[k] = v.title() if isinstance(v, Text) else v
        return out

    def removeEmptyTags(data):
        new_data = {}
        for k, v in data.items():
            if isinstance(v, dict):
                v = removeEmptyTags(v)
            if v not in (u'', None, {}):
                new_data[k] = v
        return new_data

    request = removeEmptyTags(createDict(data))
    return request


def createDict(data):
    out = {}
    for k, v in asdict(data).items():
        if hasattr(v, '__keylist__'):
            out[k] = createDict(v)
        elif isinstance(v, list) and v:
            out[k] = []
            for item in v:
                if hasattr(item, '__keylist__'):
                    out[k].append(createDict(item))
                else:
                    out[k].append(
                        str(item) if isinstance(item, Text) else item)
        else:
            if v:
                out[k] = str(v) if isinstance(v, Text) else v
    return out


def clean_empty(d):
    if not isinstance(d, (dict, list)):
        return d
    if isinstance(d, list):
        return [v for v in (clean_empty(v) for v in d) if v]
    return {k: v for k, v in ((k, clean_empty(v)) for k, v in d.items()) if v}


def cleanobject(d):
    out = {}
    for k, v in asdict(d).items():
        if k not in ('_ctiid', '_uuid') and v not in ('None', '', None):
            if hasattr(v, '__keylist__'):
                if hasattr(v, 'value'):
                    if v.value not in ('None', '', None):
                        out[k] = v.value
                else:
                    if v not in ('None', '', None):
                        out[k] = cleanobject(v)
            elif isinstance(v, list):
                out[k] = []
                for item in v:
                    if hasattr(item, '__keylist__'):
                        out[k].append(cleanobject(item))
                    else:
                        if item not in ('None', '', None):
                            out[k].append(item)
            elif isinstance(v, str):
                out[k] = v
    return out


def stripper(d):
    new_data = {}
    for k, v in d.items():
        if isinstance(v, dict):
            v = stripper(v)
        if v not in (u'', None, {}):
            new_data[k] = v
    return new_data


def clean_dict(d):
    return stripper(sobject_to_dict(d))


def escplus(number):
    if number.startswith('+'):
        return f'\{number}'
    else:
        return number
