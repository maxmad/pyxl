from pyxl.pyxl.models import BaseModel
from pyxl.pyxl.helpers import buildRequest


class User(BaseModel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def set_associated_groups(self, groups):
        if not isinstance(groups, list):
            groups = [groups]

        new_groups = {'userGroup': []}
        for group in groups:
            new_groups['userGroup'].append({'name': group})

        self.associatedGroups = new_groups

    def set_associated_devices(self, devices):
        if not isinstance(devices, list):
            devices = [devices]

        new_devices = {'device': []}
        for d in devices:
            new_devices['device'].append(d)

        self.associatedDevices = new_devices

    def set_phone_profiles(self, deviceprofiles):
        # set phone profiles and enable for cti
        if not isinstance(deviceprofiles, list):
            deviceprofiles = [deviceprofiles]

        new_devices = {'profileName': []}
        for d in deviceprofiles:
            new_devices['profileName'].append(d)

        self.phoneProfiles = new_devices
        self.ctiControlledDeviceProfiles = new_devices

    def set_primaryext(self, pattern, routePartitionName):
        """
        A users "Primary Extesion" can only be updated after a successful phone association.
        This method is currently taking the users Directory Number and Partition to
        generate the request and directly update the user in UCM.
        """

        pri_extension = {
            'pattern': pattern,
            'routePartitionName': routePartitionName}

        self.primaryExtension = pri_extension


class Phone(BaseModel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def set_phoneline(self, phoneline):
        self.lines = {'line': [buildRequest(phoneline)]}


class DeviceProfile(BaseModel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def set_phoneline(self, phoneline):
        self.lines = {'line': [buildRequest(phoneline)]}


class CallPickupGroup(BaseModel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class ConferenceBridge(BaseModel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class Css(BaseModel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class CtiRoutePoint(BaseModel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class DevicePool(BaseModel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class H323Gateway(BaseModel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class H323Trunk(BaseModel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class HuntList(BaseModel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class HuntPilot(BaseModel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class IpPhoneServices(BaseModel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class Line(BaseModel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class LineGroup(BaseModel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class Location(BaseModel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class MediaResourceGroup(BaseModel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def addMember(self, members):

        if not isinstance(members, list):
            members = [members]

        for i in members:
            member = {'deviceName': i}
            self.multicast = 'false'
            self.members.member.append(member)


class MediaResourceList(BaseModel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def addMember(self, members):

        if not isinstance(members, list):
            members = [members]

        for i in members:
            member = {'mediaResourceGroupName': i,
                      'order': str(members.index(i))}

            self.members.member.append(member)


class Mtp(BaseModel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class PhoneButtonTemplate(BaseModel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class PhoneLine(BaseModel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class PhoneSecurityProfile(BaseModel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class ProcessNode(BaseModel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class ProcessNodeService(BaseModel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class Region(BaseModel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class RemoteDestination(BaseModel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class RemoteDestinationProfile(BaseModel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class RouteGroup(BaseModel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def addMember(self, members):

        if not isinstance(members, list):
            members = [members]

        for i in members:
            member = {'deviceName': i,
                      'port': "0",
                      'deviceSelectionOrder': str(members.index(i) + 1)}

            self.members.member.append(member)


class RouteList(BaseModel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class RoutePartition(BaseModel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class RoutePattern(BaseModel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class SipProfile(BaseModel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class SipTrunk(BaseModel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class SipTrunkSecurityProfile(BaseModel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class SoftKeyTemplate(BaseModel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class Srst(BaseModel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class TimePeriod(BaseModel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class TimeSchedule(BaseModel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class Transcoder(BaseModel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)


class TransPattern(BaseModel):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)