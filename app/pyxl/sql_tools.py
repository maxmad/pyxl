from ztp import ucm


def getNumPlan(table=False):

    sql = """
    select n.pkid,
        n.dnorpattern,
        rp.name as partition,
        tp.name
    from numplan as n
    left join routepartition as rp on rp.pkid=n.fkroutepartition
    left join typepatternusage as tp on tp.enum=n.tkpatternusage
    where n.tkpatternusage in (2, 7)
    order by n.dnorpattern
    """

    return ucm.sql_query(sql, table=table)
