from pyxl.common import ProcessNode, ProcessNodeService, SipTrunk, H323Gateway
from tabulate import tabulate


def listH323Gateways(tab=False):

    item = H323Gateway()
    searchCriteria = {'name': '%'}
    items = item.list(searchCriteria=searchCriteria)

    out = []
    for x in items:
        item._uuid = x._uuid
        item.get()

        trunk = {'name': item.name,
                 'description': item.description,
                 'DevicePool': '',
                 'CSS_In': ''}

        if item.callingSearchSpaceName:
            trunk['CSS_In'] = item.callingSearchSpaceName.value

        if item.devicePoolName:
            trunk['DevicePool'] = item.devicePoolName.value

        out.append(trunk)

    if tab:
        return print(tabulate(out, headers='keys', tablefmt="pipe"))
    else:
        return out


def listSiptrunks(tab=False):

    item = SipTrunk()
    searchCriteria = {'name': '%'}
    items = item.list(searchCriteria=searchCriteria)

    out = []
    for x in items:
        item._uuid = x._uuid
        item.get()
        trunk = {'name': item.name,
                 'description': item.description,
                 'DevicePool': '',
                 'CSS_In': '',
                 'sipProfileName': item.sipProfileName.value,
                 'destinations': []}

        for d in item.destinations.destination:
            trunk['destinations'].append(d.addressIpv4)

        if item.callingSearchSpaceName:
            trunk['CSS_In'] = item.callingSearchSpaceName.value

        if item.devicePoolName:
            trunk['DevicePool'] = item.devicePoolName.value

        if item.rerouteCallingSearchSpaceName:
            trunk['RerouteCSS'] = item.rerouteCallingSearchSpaceName.value

        out.append(trunk)

    if tab:
        return print(tabulate(out, headers='keys', tablefmt="pipe"))
    else:
        return out


def listServer(tab=False):
    # print(tabulate(listServer(), headers='keys'))
    pn = ProcessNode()
    nodes = pn.list(searchCriteria={'name': '%'})

    out = []
    for x in nodes:
        p = ProcessNode(uuid=x['_uuid'])
        _node = p.get()
        if _node['name'] != "EnterpriseWideData":
            node = {'name': _node['name'], 'nodeUsage': _node['nodeUsage'],
                    'processNodeRole': _node['processNodeRole']}
            out.append(node)

    out = sorted(out, key=lambda serv: serv['processNodeRole'])

    if tab:
        return print(tabulate(out, headers='keys', tablefmt="pipe"))
    else:
        return out


def listServices(node=None, tab=False):
    """
    services = listServices('nodename')
    print(tabulate(services, headers='keys'))
    """
    if not node:
        _node = '%'
    else:
        _node = node

    pn = ProcessNodeService()
    _service = pn.list(searchCriteria={'processNodeName': _node})

    out = []
    for x in _service:
        p = ProcessNodeService(uuid=x['_uuid'])
        _service = p.get()
        if _service['service'] != 'Enterprise Wide':
            service = {'service': _service['service'],
                       'isActive': _service['isActive']}
            out.append(service)

    out = sorted(out, key=lambda serv: serv['service'])

    if tab:
        return print(tabulate(out, headers='keys', tablefmt="pipe"))
    else:
        return out

    return out
