from copy import copy
from pyxl import ucm
from pyxl.helpers import buildRequest, gen_response, response, recursive_asdict, dict_modified, get_module_logger

from suds.sudsobject import Object

client = ucm.client

log = get_module_logger(__name__)
log.debug(f'module {__name__} initiated.')


class BaseModel(Object):

    __original__ = ''

    def __init__(self, **kwargs):
        """
        Mapper Class for AXL Schema
        """

        __fac__ = client.factory.create(f'ns0:X{self.__class__.__name__}')

        self.__original__ = client.factory.create(
            f'ns0:X{self.__class__.__name__}')

        self.__dict__.update(__fac__.__dict__)

        for key, value in kwargs.items():
            setattr(self, key, copy(value))

    def factory(self):
        return client.factory.create(f'ns0:X{self.__class__.__name__}')

    def get(self, returnedTags=None):
        method = getattr(client.service, f'get{self.__class__.__name__}')

        if returnedTags:
            if hasattr(self, '_uuid'):
                result = gen_response(
                    method(uuid=self._uuid, returnedTags=returnedTags))
            else:
                result = gen_response(
                    method(returnedTags=returnedTags, **buildRequest(self)))
        else:
            if hasattr(self, '_uuid'):
                result = gen_response(method(uuid=self._uuid))
            else:
                result = gen_response(method(**buildRequest(self)))

        if hasattr(result, '_uuid'):
            for key, value in result.__dict__.items():
                setattr(self, key, copy(value))
                self.__original__.__dict__[key] = copy(value)

            return result

    def update(self, dry=False):
        method = getattr(client.service,
                         f'update{self.__class__.__name__}')

        old = recursive_asdict(self.__original__)
        new = recursive_asdict(self)
        delta = dict_modified(old, new)

        if delta:
            if dry:
                return delta
            else:
                log.info(
                    f'UCM:UPDATED:{self.__class__.__name__}:{self._uuid}: {delta}')
                result = response(method(uuid=self._uuid, **delta))
                if result:
                    for k, v in delta.items():
                        self.__original__.__dict__[k] = copy(v)
                return result
        else:
            log.info(
                f'UCM:{self.__class__.__name__}:{self._uuid} up to date...')
            return

    def remove(self):
        method = getattr(client.service,
                         f'remove{self.__class__.__name__}')

        if hasattr(self, '_uuid'):
            result = response(method(uuid=self._uuid))
        else:
            result = response(method(**buildRequest(self)))

        if result:
            log.info(f'UCM:REMOVED:{self.__class__.__name__}:{result}')

        return result

    def add(self):
        method = getattr(client.service, f'add{self.__class__.__name__}')
        log.debug('Calling SOAP method.')
        result = response(method(buildRequest(self)))
        if result:
            log.info(f'UCM:ADDED:{self.__class__.__name__}:{result}')
            self._uuid = result
        return result

    def list(self, searchCriteria, returnedTags=None):
        method = getattr(client.service, f'list{self.__class__.__name__}')
        result = response(
            method(searchCriteria=searchCriteria, returnedTags=returnedTags))
        return result
