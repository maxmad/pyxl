import logging
import os
import requests
from requests.packages.urllib3.exceptions import InsecureRequestWarning

from suds.client import Client
from suds.xsd.doctor import Import
from suds.xsd.doctor import ImportDoctor

from cryptography import x509
from cryptography.hazmat.backends import default_backend
from tabulate import tabulate
import xml.etree.ElementTree as ET

from pyxl.client_transport import RequestsHttpTransport
from pyxl.helpers import clean_dict, next_response

cucmhost = os.environ['CUCM_HOST']
cucmuser = os.environ['CUCM_USER']
cucmpass = os.environ['CUCM_PASS']
cucmvers = os.environ['CUCM_VERSION']

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


def get_module_logger(mod_name):
    """
    To use this, do logger = get_module_logger(__name__)
    """
    logger = logging.getLogger(mod_name)
    handler = logging.StreamHandler()
    formatter = logging.Formatter('%(asctime)s %(levelname)-8s %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(logging.INFO)
    return logger


def disable_logging():
    logging.disable(logging.ERROR)
    logging.disable(logging.WARNING)
    logging.disable(logging.DEBUG)
    logging.disable(logging.INFO)
    logging.disable(logging.CRITICAL)


def enable_logging():
    import logging
    from http.client import HTTPConnection

    HTTPConnection.debuglevel = 1

    logging.basicConfig(level=logging.INFO)
    requests_log = logging.getLogger("requests.packages.urllib3")
    requests_log.setLevel(logging.DEBUG)
    requests_log.propagate = True


log = get_module_logger(__name__)
log.info(f'module {__name__} initiated.')


class AXL(object):

    def __init__(
        self,
        host=cucmhost,
        username=cucmuser,
        password=cucmpass,
        cucm_version=cucmvers,
        timeout=10,
    ):

        self.username = username
        self.password = password
        self.host = host
        self.timeout = timeout

        wsdl_path = os.path.abspath(f'axl_wsdl/{cucmvers}/AXLAPI.wsdl')
        wsdl = "file://" + wsdl_path

        session = requests.Session()
        session.auth = (cucmuser, cucmpass)
        session.verify = False
        session.stream = True

        imp = Import('http://schemas.xmlsoap.org/soap/encoding/',
                     'http://schemas.xmlsoap.org/soap/encoding/')

        imp.filter.add('http://schemas.cisco.com/ast/soap/')

        try:
            log.debug(
                f'Building SOAP Client for host {host} and WSDL version {cucmvers}.')
            self.client = Client(
                wsdl,
                location=f'https://{host}:8443/axl/',
                faults=False,
                plugins=[ImportDoctor(imp)],
                transport=RequestsHttpTransport(session),
                timeout=self.timeout
            )
        except Exception as e:
            log.error(e)
            raise e

        self.service = self.client.service

    def sql_query(self, sql, table=False):
        resp = self.client.service.executeSQLQuery(sql)

        result = {
            'success': False,
            'response': '',
            'error': '',
        }

        if resp[0] == 200:
            result['success'] = True
            result['response'] = 'EMPTY'
            if 'row' not in resp[1]['return']:
                return result
            else:
                output = []
                row = {}
                keylist = resp[1]['return']['row'][0].__keylist__
                for i in resp[1]['return']['row']:
                    row = {}
                    for key in keylist:
                        row[key] = ''
                        row[key] = i.__getattribute__(key)
                    output.append(row)
                    result['response'] = output

            if table:
                print(tabulate(result['response'], headers='keys'))
            else:
                return result
        elif resp[0] == 500 and 'syntax error' in resp[1].faultstring:
            result['response'] = 'SQL Query: {0} ehh..syntax error'.format(sql)
            result['error'] = resp[1].faultstring
            return result
        else:
            result['response'] = 'Unknown error'
            result['error'] = resp[1].faultstring
            return result

    def RObject(self, action, xtype):
        try:
            ['List', 'Get', 'Add', 'Update', 'Remove'].index(action)
        except Exception as e:
            print('wrong action, try: "List", "Get", "Add", "Update", "Remove"')
        else:
            try:
                response = self.client.factory.create(
                    'ns0:{0}{1}Req'.format(action, xtype))
            except Exception as e:
                response = None
            return response

    def ListObject(self, listobject):

        # ugly
        xtype = str(listobject.__class__)[28:-5]

        try:
            res = self.client.factory.create(f'ns0:List{xtype}Res')
        except Exception as e:
            raise e

        rname = res['return'].__keylist__[0]

        method = 'self.client.service.' + 'list' + xtype

        req = clean_dict(listobject)

        try:
            response = eval(method)(req['searchCriteria'], req['returnedTags'])[
                1]['return'][rname]
        except Exception as e:
            print(rname, 'not found ... because: ', e)
            response = None

        return response

    def GetObject(self, getobject, full=False):

        if str(getobject.__class__)[24:].startswith('Get'):
            xtype = str(getobject.__class__)[27:-5]  # L Type ... GetMtp
        elif str(getobject.__class__)[24:].startswith(('R', 'L')):
            xtype = str(getobject.__class__)[25:-2]  # Get Type ... RMtp/LMTP

        try:
            res = self.client.factory.create(
                'ns0:Get{0}Res'.format(xtype))
        except Exception as e:
            raise e

        rname = res['return'].__keylist__[0]

        method = 'self.client.service.' + 'get' + xtype

        try:
            response = eval(method)(uuid=getobject._uuid)[1]['return'][rname]
        except Exception as e:
            raise e

        return response

    def get_certificates(self, full=False, table=True):
        certs = []
        sql = '''
            SELECT CERTIFICATE.certificate,
                   CERTIFICATE.pkid,
                   CERTIFICATE.serialnumber,
                   CERTIFICATEPROCESSNODEMAP.ipv4address,
                   CERTIFICATEPROCESSNODEMAP.servername,
                   TYPECERTIFICATESERVICE.name,
                   subjectname
            FROM CERTIFICATEPROCESSNODEMAP
            INNER JOIN CERTIFICATE ON CERTIFICATEPROCESSNODEMAP.fkcertificate=CERTIFICATE.pkid
            INNER JOIN CERTIFICATESERVICECERTIFICATEMAP ON CERTIFICATESERVICECERTIFICATEMAP.fkcertificate=CERTIFICATE.pkid
            JOIN TYPECERTIFICATESERVICE ON TYPECERTIFICATESERVICE.enum=CERTIFICATESERVICECERTIFICATEMAP.tkcertificateservice
            ORDER BY TYPECERTIFICATESERVICE.name
            '''

        try:
            response = self.sql_query(sql)['response']
        except Exception as e:
            print(e)
            return print("{0}".format(e))

        for cert in response:
            pem_cert = x509.load_pem_x509_certificate(
                cert['certificate'].encode('UTF-8'), default_backend())
            begin = pem_cert.not_valid_before.isoformat()
            end = pem_cert.not_valid_after.isoformat()
            if full:
                certs.append([
                    cert['certificate'],
                    cert['name'],
                    cert['servername'],
                    cert['ipv4address'],
                    cert['serialnumber'],
                    begin,
                    end
                ])
            else:
                certs.append([
                    cert['name'],
                    cert['servername'],
                    cert['ipv4address'],
                    cert['serialnumber'],
                    begin,
                    end
                ])

        if table:
            headers = ['name', 'servername', 'ipv4address',
                       'serialnumber', 'begin', 'end']
            return tabulate(certs, headers, tablefmt="pipe")

        return certs

    def get_service_parameter(self):
        req = self.RObject('List', 'ServiceParameter')
        req.searchCriteria.processNodeName = "%"
        req.returnedTags.processNodeName = "."
        req.returnedTags.name = "."
        req.returnedTags.service = "."
        req.returnedTags.value = "."
        req.returnedTags.valueType = "."

        lres = self.ListObject(req)

        return lres

    def get_changed_params(self, table=True):
        def_params = []
        current_params = []
        changes = []

        # parse defaults from xml
        sql = 'SELECT name, content FROM Scratch WHERE name like "xmlrule://cisco.com/ServiceParameter%"'
        res = self.sql_query(sql)

        for i in res['response']:
            service = i['name'].split('/')[-2]
            param = i['name'].split('/')[-1]
            xml = i['content']
            root = ET.fromstring(xml)
            for val in root.iter('default'):
                value = val.text
            for val in root.iter('defaultKey'):
                value = val.text
            if value == 'true':
                value = 'True'
            if value == 'false':
                value = 'False'
            def_params.append(
                {'param': param, 'value': value, 'service': service})

        s = self.get_service_parameter()
        for i in s:
            value = i.value
            # bug ?
            if value == 'T' and i.name != 'NameDisplayForOriginalDialedNumberWhenTranslated':
                value = 'True'
            if value == 'F':
                value = 'False'
            current_params.append(
                {'server': i.processNodeName.value, 'param': i.name, 'value': value, 'service': i.service})

        for d in def_params:
            for c in current_params:
                if c['param'] == d['param']:
                    if c['value'] != d['value']:
                        # print(tabulate(changes, headers=['parameter',
                        # 'current', 'default', 'service', 'server']))
                        changes.append([c['param'], c['value'], d[
                                       'value'], c['service'], c['server']])

        if table:
            headers = ['parameter', 'current', 'default', 'service', 'server']
            return tabulate(changes, headers, tablefmt="pipe")

        return changes

    def first_listChange(self):
        try:
            response = self.client.service.listChange()[1]['queueInfo']
        except Exception as e:
            raise(e)

        return response

    def listChange(self, lastChangeId, queueId=None, objects=None):
        # param: objects
        # "User"
        # "Phone"
        # ["User", "Phone", "Line"]
        #
        # response:
        # 2:Phone: 70938998-19ff-72b1-8ec2-f5aed34c465a - a

        listChange = self.client.factory.create('ns0:listChange')

        listChange.startChangeId.value = lastChangeId

        if queueId:
            listChange.startChangeId._queueId = queueId
        else:
            first = self.first_listChange()
            listChange.startChangeId._queueId = first.queueId

        if objects:
            if not isinstance(objects, list):
                objects = [objects]
            for o in objects:
                listChange.objectList.object.append(o)

        try:
            response = self.client.service.listChange(
                listChange.startChangeId, listChange.objectList)
        except Exception as e:
            raise(e)

        response = next_response(response)

        # changes
        if response:
            if response.changes:
                for change in response.changes.change:
                    print(
                        f'{change.id}:{change._type}: {change._uuid} - {change.action}')
            else:
                print("no changes...")

        return response


"""
from pyxl import AXL
ucm = AXL()
ucm.test_client(ucm)
...
from pyxl import AXL
ucm = AXL()
print(ucm.get_certificates())
"""
